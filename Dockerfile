# Troels - s120052
FROM adoptopenjdk:8-jre-hotspot
WORKDIR /usr/src
COPY target/02267_CustomerController-1.0-thorntail.jar /usr/src
CMD java -Xmx64m \
    -Djava.net.preferIPv4Stack=true \
    -Djava.net.preferIPv4Addresses=true \
    #-Dthorntail.host.port=8081 \
    -jar 02267_CustomerController-1.0-thorntail.jar