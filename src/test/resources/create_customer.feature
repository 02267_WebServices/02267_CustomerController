Feature: Create new customer
  Scenario: Customer successfully creates new profile
    Given a customer with no existing account
    When the customer creates a new account
    Then the new account is saved in the customer database