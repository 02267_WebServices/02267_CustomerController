Feature: Request tokens
  Scenario: Customer requests 6 new tokens
    Given a customer with no tokens
    When the customer requests 6 tokens
    Then the "TokensRequested" event is broadcast
    When the "TokensRequestAccepted" event is received
    Then the customer receives the requested amount of unique tokens