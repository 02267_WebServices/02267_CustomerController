Feature: Delete customer
  Scenario: An existing customer deletes their account
    Given a database with an existing customer account
    When the customer deletes their account
    Then the account is removed from the customer database