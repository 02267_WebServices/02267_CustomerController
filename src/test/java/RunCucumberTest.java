// this file runs all the cucumber tests

import org.junit.runner.RunWith;

import io.cucumber.junit.CucumberOptions;
import io.cucumber.junit.Cucumber;

/**
 * @author August Birk Olsen - s154110
 */
@RunWith(Cucumber.class)
@CucumberOptions(plugin = { "pretty" }, features = "src/test/resources")
public class RunCucumberTest {
}
