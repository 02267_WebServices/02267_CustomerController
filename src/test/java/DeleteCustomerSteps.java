import CustomerService.Customer;
import CustomerService.CustomerController;
import CustomerService.CustomerDatabase;
import CustomerService.exceptions.CustomerNotFoundException;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import static org.junit.Assert.assertTrue;
import messaging.rabbitmq.RabbitMQAdapter;
import messaging.rabbitmq.RabbitMqSender;

/**
 * @author August Birk Olsen - s154110
 */
public class DeleteCustomerSteps {
    CustomerDatabase customerDatabase = new CustomerDatabase();
    CustomerController customerController = new CustomerController(customerDatabase, new RabbitMQAdapter(new RabbitMqSender()));
    Customer customer;
    String customerId;

    @Given("a database with an existing customer account")
    public void aDatabaseWithAnExistingCustomerAccount() {
        customer = new Customer("012345-0123", "Johnny", "Dee", "123456");
        customerId = customerController.createCustomer(customer.getCpr(), customer.getFirstName(), customer.getLastName(), customer.getBankAccount());
    }

    @When("the customer deletes their account")
    public void theCustomerDeletesTheirAccount() throws CustomerNotFoundException {
        customerController.deleteCustomer(customerId);
    }

    @Then("the account is removed from the customer database")
    public void theAccountIsRemovedFromTheCustomerDatabase() {
        boolean customerNotFound = false;
        try {
            customerController.getCustomer(customerId);
        } catch (CustomerNotFoundException e) {
            customerNotFound = true;
        }
        assertTrue(customerNotFound);
    }
}
