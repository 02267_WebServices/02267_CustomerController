import CustomerService.Customer;
import CustomerService.CustomerDatabase;
import CustomerService.exceptions.CustomerNotFoundException;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * @author August Birk Olsen - s154110
 */
public class CustomerDatabaseTest
{

    CustomerDatabase customerDatabase;
    String cpr = "020995-1234",
            firstname = "Hugh",
            lastname = "Yackman",
            bankAccount = "BankId";
    Customer customer;

    @Before
    public void init() throws Exception
    {
        customer = new Customer(cpr, firstname, lastname, bankAccount);
        customerDatabase = new CustomerDatabase();
    }

    @Test
    public void addCustomer()
    {
        customerDatabase.addCustomer(customer);
        assertEquals(1, customerDatabase.getNumberOfCustomers());
    }

    @Test
    public void removeCustomer() throws CustomerNotFoundException
    {
        customerDatabase.addCustomer(customer);
        assertEquals(1, customerDatabase.getNumberOfCustomers());
        customerDatabase.removeCustomer(customer.getId());
        assertEquals(0, customerDatabase.getNumberOfCustomers());
    }

    @Test
    public void getCustomer() throws CustomerNotFoundException
    {
        customerDatabase.addCustomer(customer);
        Customer verify_customer = customerDatabase.getCustomer(customer.getId());
        assertEquals(cpr, verify_customer.getCpr());
        assertEquals(firstname, verify_customer.getFirstName());
        assertEquals(lastname, verify_customer.getLastName());
        assertEquals(bankAccount, verify_customer.getBankAccount());
    }
}