import CustomerService.Customer;
import org.junit.Before;
import org.junit.Test;

import java.util.UUID;

import static org.junit.Assert.*;

/**
 * @author August Birk Olsen - s154110
 */
public class CustomerTest
{
    String cpr, firstname, lastname, bankAccount;
    Customer customer;

    @Before
    public void init()
    {
        cpr = "123456-8911";
        firstname = "Man";
        lastname = "McMan";
        bankAccount = "3945-3941-1020-2034";
        customer = new Customer(cpr, firstname, lastname, bankAccount);
    }

    @Test
    public void getCpr()
    {
        assertEquals(cpr, customer.getCpr());
    }

    @Test
    public void getFirstName()
    {
        assertEquals(firstname, customer.getFirstName());
    }

    @Test
    public void getLastName()
    {
        assertEquals(lastname, customer.getLastName());
    }

    @Test
    public void getBankAccount()
    {
        assertEquals(bankAccount, customer.getBankAccount());
    }

    @Test
    public void getCustomerUUIDTest() { assertEquals(UUID.randomUUID().toString().length(), customer.getId().length()); }
}