import CustomerService.Customer;
import CustomerService.CustomerController;
import CustomerService.CustomerDatabase;
import CustomerService.exceptions.CustomerNotFoundException;
import messaging.rabbitmq.EventSender;
import messaging.rabbitmq.RabbitMQAdapter;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.*;

import static org.junit.Assert.*;

/**
 * @author August Birk Olsen - s154110
 */
public class CustomerControllerTest
{
    CustomerDatabase customers;
    RabbitMQAdapter rmqAdapter;
    EventSender sender;
    CustomerController customerController;
    String customerUUID;

    String cpr = "020995-1234",
            firstname = "Hugh",
            lastname = "Yackman",
            bankAccount = "BankId";

    @Before
    public void setUp() throws Exception
    {
        sender = mock(EventSender.class);
        customers = new CustomerDatabase();
        rmqAdapter = new RabbitMQAdapter(sender);
        customerController = new CustomerController(customers, rmqAdapter);
    }

    @Test
    public void createAndGetCustomer() throws CustomerNotFoundException
    {
        customerUUID = customerController.createCustomer(cpr, firstname, lastname, bankAccount);
        Customer customer = customers.getCustomer(customerUUID);
        assertEquals(customerUUID, customer.getId());
        assertEquals(cpr, customer.getCpr());
        assertEquals(firstname, customer.getFirstName());
        assertEquals(lastname, customer.getLastName());
        assertEquals(bankAccount, customer.getBankAccount());
    }

    @Test
    public void deleteCustomer() throws CustomerNotFoundException
    {
        customerUUID = customerController.createCustomer(cpr, firstname, lastname, bankAccount);
        assertEquals(customerUUID, customers.getCustomer(customerUUID).getId());
        customerController.deleteCustomer(customerUUID);
        try{
            customers.getCustomer(customerUUID);
        } catch (CustomerNotFoundException e) {
            assertEquals("No customer was found with the id: "+customerUUID, e.getMessage());
        }
    }

    @Test
    public void requestTokens() throws Exception
    {
        // This is not really ideal
        List<String> list = new ArrayList<>();
        list.add("1"); list.add("2"); list.add("3");

        CustomerController mockCustomerController = mock(CustomerController.class);
        when(mockCustomerController.requestTokens(customerUUID, 3)).thenReturn(list);

        List<String> tokens = mockCustomerController.requestTokens(customerUUID, 3);

        assertEquals(list, tokens);
    }
}