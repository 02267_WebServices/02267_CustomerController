import CustomerService.Customer;
import CustomerService.CustomerController;
import CustomerService.CustomerDatabase;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import messaging.rabbitmq.Event;
import messaging.rabbitmq.EventSender;
import messaging.rabbitmq.RabbitMQAdapter;
import org.assertj.core.util.Arrays;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CompletableFuture;

import static org.junit.Assert.assertEquals;

/**
 * @author August Birk Olsen - s154110
 */
public class RequestTokensSteps {
    CustomerDatabase customerDatabase = new CustomerDatabase();
    RabbitMQAdapter rabbitMQAdapter;
    CustomerController customerController;
    EventSender eventSender;
    CompletableFuture<Event> sentEvent = new CompletableFuture<>();
    CompletableFuture<List<String>> response = new CompletableFuture<>();
    Customer customer;
    String customerId;
    List<String> tokens;
    int numTokens;

    public RequestTokensSteps() {
        eventSender = new EventSender() {
            @Override
            public void sendEvent(Event event) throws Exception {
                sentEvent.complete(event);
            }
        };
        rabbitMQAdapter = new RabbitMQAdapter(eventSender);
        customerController = new CustomerController(customerDatabase, rabbitMQAdapter);
    }

    @Given("a customer with no tokens")
    public void aCustomerWithNoTokens() {
        customer = new Customer("012345-0123", "Johnny", "Dee", "123456");
        customerId = customerController.createCustomer(customer.getCpr(), customer.getFirstName(), customer.getLastName(), customer.getBankAccount());
    }

    @When("the customer requests {int} tokens")
    public void theCustomerRequestsTokens(int arg0) throws Exception {
        numTokens = arg0;
        new Thread(() -> {
            try {
                response.complete(customerController.requestTokens(customerId, arg0));
            } catch (Exception e) {
                throw new Error(e);
            }
        }).start();
    }

    @Then("the {string} event is broadcast")
    public void theEventIsBroadcast(String arg0) {
        assertEquals(arg0, sentEvent.join().getEventType());
    }

    @When("the {string} event is received")
    public void theEventIsReceived(String arg0) throws Exception {
        List<String> tokens = new ArrayList<>();
        for (int i = 0; i < numTokens; i++) {
            tokens.add(String.valueOf(i));
        }
        Object[] args = Arrays.asObjectArray(tokens.toArray());
        Event event = new Event(arg0, args);
        rabbitMQAdapter.receiveEvent(event);
    }

    @Then("the customer receives the requested amount of unique tokens")
    public void theCustomerReceivedUniqueTokens() {
        tokens = response.join();
        Set<String> uniqueTokens = new HashSet<>(tokens);
        assertEquals(numTokens, uniqueTokens.size());
    }
}
