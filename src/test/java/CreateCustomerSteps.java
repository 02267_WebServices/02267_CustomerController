import CustomerService.Customer;
import CustomerService.CustomerController;
import CustomerService.CustomerDatabase;
import CustomerService.exceptions.CustomerNotFoundException;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import messaging.rabbitmq.RabbitMQAdapter;
import messaging.rabbitmq.RabbitMqSender;

/**
 * @author August Birk Olsen - s154110
 */
public class CreateCustomerSteps {
    CustomerDatabase customerDatabase = new CustomerDatabase();
    CustomerController customerController = new CustomerController(customerDatabase, new RabbitMQAdapter(new RabbitMqSender()));
    Customer customer;
    String customerId;

    @Given("a customer with no existing account")
    public void aCustomerWithNoExistingAccount() {
        customer = new Customer("012345-0123", "Johnny", "Dee", "123456");
    }

    @When("the customer creates a new account")
    public void theCustomerCreatesANewAccount() {
        customerId = customerController.createCustomer(customer.getCpr(), customer.getFirstName(), customer.getLastName(), customer.getBankAccount());
    }

    @Then("the new account is saved in the customer database")
    public void theNewAccountIsSavedInTheCustomerDatabase() throws CustomerNotFoundException {
        customerController.getCustomer(customerId);
    }
}
