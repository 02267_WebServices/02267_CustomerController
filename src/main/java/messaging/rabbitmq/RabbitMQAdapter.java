package messaging.rabbitmq;

import CustomerService.Customer;
import CustomerService.CustomerController;

import CustomerService.exceptions.CustomerNotFoundException;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;

/**
 * @author August Birk Olsen - s154110
 */
public class RabbitMQAdapter implements EventReceiver {
    private static final String TOKENS_REQUESTED = "TokensRequested";
    private static final String TOKENS_REQUEST_ACCEPTED = "TokensRequestAccepted";
    private static final String TOKENS_REQUEST_DENIED = "TokensRequestDenied";
    private static final String TOKEN_IS_VALID = "TokenIsValid";
    private static final String BANK_ACCOUNT_FROM_TOKEN = "BankAccountFromToken";
    private static final String USER_TYPE_REQUEST = "UserTypeRequest";
    private static final String USER_TYPE_IDENTIFIED = "UserTypeIdentified";
    private static final String USER_TYPE_IS_CUSTOMER = "customer";

    private CustomerController customerController;
    private CompletableFuture<Event> future;
    private EventSender sender;

    /**
     * @param sender an event sender
     */
    public RabbitMQAdapter(EventSender sender) {
        this.sender = sender;
    }

    /**
     * @param customerController contains the logic for the customer
     */
    public void setController(CustomerController customerController) {
        this.customerController = customerController;
    }

    /**
     * @param id UUID of a token
     * @param numTokens amount of tokens requested
     * @return a list of token UUIDs
     * @throws Exception if the response is not correct
     */
    public List<String> requestTokens(String id, int numTokens) throws Exception {
        Object[] args = {id, numTokens};
        Event event = new Event(TOKENS_REQUESTED, args);
        future = new CompletableFuture<>();
        sender.sendEvent(event);
        Event response = future.join();
        if (TOKENS_REQUEST_DENIED.equals(response.getEventType())) {
            throw new Exception("Token request denied");
        }
        List<String> tokenIds = new ArrayList<>();
        for (Object tokenId : response.getArguments()) {
            tokenIds.add(tokenId.toString());
        }
        return tokenIds;
    }

    /**
     * @param id UUID of the customer
     * @throws Exception CustomerNotFoundException
     */
    public void customerAccountRequested(String id) throws Exception {
        Customer customer = customerController.getCustomer(id);
        Object[] args = {id, customer.getBankAccount()};
        Event event = new Event(BANK_ACCOUNT_FROM_TOKEN, args);
        sender.sendEvent(event);
    }

    /**
     * @param event receive event in the form of Event(String EventType, Object[] args)
     * @throws Exception Exception
     * <p> This method listens for TOKENS_REQUEST_ACCEPTED, TOKEN_IS_VALID, and USER_TYPE_REQUEST</p>
     */
    public void receiveEvent(Event event) throws Exception {
        if (TOKENS_REQUEST_ACCEPTED.equals(event.getEventType()) || TOKENS_REQUEST_DENIED.equals(event.getEventType())) {
            future.complete(event);
        }
        if (TOKEN_IS_VALID.equals(event.getEventType())) {
            String id = event.getArguments()[0].toString();
            customerAccountRequested(id);
        }
        if (USER_TYPE_REQUEST.equals(event.getEventType())) {
            String userId = event.getArguments()[0].toString();
            try {
                customerController.getCustomer(userId);
                Object[] args = { userId, USER_TYPE_IS_CUSTOMER };
                Event userTypeEvent = new Event(USER_TYPE_IDENTIFIED, args);
                sender.sendEvent(userTypeEvent);
            } catch (CustomerNotFoundException e) {}
        }
    }
}
