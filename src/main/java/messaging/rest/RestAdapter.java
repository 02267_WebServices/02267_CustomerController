package messaging.rest;

import CustomerService.CustomerController;
import CustomerService.CustomerControllerFactory;
import CustomerService.exceptions.CustomerNotFoundException;
import messaging.rest.dto.CustomerRepresentation;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.util.List;

/**
 * @author August Birk Olsen - s154110
 */
@Path("/customer")
public class RestAdapter {
    CustomerController customerController = new CustomerControllerFactory().getCustomerController();

    @POST
    @Path("/customer")
    @Consumes("application/json")
    public Response createCustomer(CustomerRepresentation c) {
        String customerId = customerController.createCustomer(c.cpr, c.firstName, c.lastName, c.bankAccountId);
        return Response.status(Response.Status.CREATED.getStatusCode()).entity(customerId).build();
    }

    @DELETE
    @Path("/customer")
    @Consumes("text/plain")
    public Response deleteCustomer(@QueryParam("id") String id) {
        try {
            customerController.deleteCustomer(id);
        } catch (CustomerNotFoundException e) {
            return Response.status(Response.Status.NOT_FOUND).entity(e.getMessage()).build();
        }
        return Response.ok().build();
    }

    @GET
    @Path("/tokens")
    @Produces("application/json")
    public Response requestTokens(@QueryParam("id") String id, @DefaultValue("1") @QueryParam("amount") int numTokens) {
        try {
            List<String> tokens = customerController.requestTokens(id, numTokens);
            return Response.ok(tokens).build();
        } catch (Exception e) {
            return Response.status(Response.Status.BAD_REQUEST).entity(e.getMessage()).build();
        }
    }

    @GET
    @Path("/test")
    @Produces("text/plain")
    public Response testConnection() {
        return Response.ok("Connected to Thorntail!").build();
    }
}
