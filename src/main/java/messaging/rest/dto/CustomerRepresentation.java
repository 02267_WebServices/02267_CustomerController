package messaging.rest.dto;

/**
 * @author August Birk Olsen - s154110
 */
public class CustomerRepresentation {
    public String cpr, firstName, lastName, bankAccountId;
}
