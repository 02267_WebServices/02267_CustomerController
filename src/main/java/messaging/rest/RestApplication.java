package messaging.rest;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 * @author August Birk Olsen - s154110
 */
@ApplicationPath("/")
public class RestApplication extends Application {

}
