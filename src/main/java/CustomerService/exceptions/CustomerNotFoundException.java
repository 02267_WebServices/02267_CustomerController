package CustomerService.exceptions;

/**
 * @author August Birk Olsen - s154110
 */
public class CustomerNotFoundException extends Exception {

    public CustomerNotFoundException() {
        super();
    }

    public CustomerNotFoundException(String message) {
        super(message);
    }
}
