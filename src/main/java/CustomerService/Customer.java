package CustomerService;

import java.util.UUID;

/**
 * @author August Birk Olsen - s154110
 */
public class Customer {

    private String id, cpr, firstName, lastName, bankAccount;

    /**
     * @param cpr unique identification of the customer
     * @param firstName first name of the customer
     * @param lastName last name of the customer
     * @param bankAccount bank account identification of the customer
     * <p> Note: This constructor also creates a UUID for the customer </p>
     */
    public Customer(String cpr, String firstName, String lastName, String bankAccount) {
        this.cpr = cpr;
        this.firstName = firstName;
        this.lastName = lastName;
        this.bankAccount = bankAccount;
        id = UUID.randomUUID().toString();
    }

    /**
     * @return the UUID of the customer
     */
    public String getId() {
        return id;
    }

    /**
     * @return the cpr number of the customer
     */
    public String getCpr() {
        return cpr;
    }

    /**
     * @return the first name of the customer
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * @return the last name of the customer
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * @return the bank account identification of a customer
     */
    public String getBankAccount() {
        return bankAccount;
    }

}
