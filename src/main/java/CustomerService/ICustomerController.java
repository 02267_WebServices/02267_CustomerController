package CustomerService;

import CustomerService.exceptions.CustomerNotFoundException;

import java.util.List;

/**
 * @author August Birk Olsen - s154110
 */
public interface ICustomerController {

    String createCustomer(String cpr, String firstName, String lastName, String bankAccount);

    /**
     * @param id unique ID of a customer
     * @throws CustomerNotFoundException customer was not found
     */
    void deleteCustomer(String id) throws CustomerNotFoundException;

    /**
     * @param id unique ID of a customer
     * @return the customer with the given ID
     * @throws CustomerNotFoundException customer was not found
     */
    Customer getCustomer(String id) throws CustomerNotFoundException;

    /**
     * @param id UUID of a customer
     * @param numTokens amount of tokens requested
     * @return list of UUID of tokens
     * @throws Exception Exception
     */
    List<String> requestTokens(String id, int numTokens) throws Exception;
}
