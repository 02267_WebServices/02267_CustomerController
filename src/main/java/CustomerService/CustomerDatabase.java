package CustomerService;

import CustomerService.exceptions.CustomerNotFoundException;

import java.util.HashMap;

/**
 * @author August Birk Olsen - s154110
 */
public class CustomerDatabase implements ICustomerDatabase {

    private HashMap<String, Customer> customers = new HashMap<>();

    /**
     * @param customer a customer to be added to the database
     */
    public void addCustomer(Customer customer) {
        customers.put(customer.getId(), customer);
    }

    /**
     * @param id UUID of a customer to be removed from the database
     * @throws CustomerNotFoundException a customer could not be found
     */
    public void removeCustomer(String id) throws CustomerNotFoundException {
        Customer removed = customers.remove(id);
        if (removed == null) {
            throw new CustomerNotFoundException("No customer was found with the id: "+ id);
        }
    }

    /**
     * @param id UUID of a customer
     * @return the customer with the given UUID
     * @throws CustomerNotFoundException a customer could not be found
     */
    public Customer getCustomer(String id) throws CustomerNotFoundException {
        Customer customer = customers.get(id);
        if (customer == null) {
            throw new CustomerNotFoundException("No customer was found with the id: "+ id);
        }
        return customer;
    }

    /**
     * @return the numbers of customers within the database
     */
    @Override
    public int getNumberOfCustomers()
    {
        return customers.size();
    }
}
