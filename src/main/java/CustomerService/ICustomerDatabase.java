package CustomerService;

import CustomerService.exceptions.CustomerNotFoundException;

/**
 * @author August Birk Olsen - s154110
 */
public interface ICustomerDatabase {
    /**
     * @param customer a customer object
     */
    void addCustomer(Customer customer);

    /**
     * @param id UUID of a customer
     * @throws CustomerNotFoundException a customer was not found
     */
    void removeCustomer(String id) throws CustomerNotFoundException;

    /**
     * @param id UUID of a customer
     * @return a customer with the given UUID
     * @throws CustomerNotFoundException a customer was not found
     */
    Customer getCustomer(String id) throws CustomerNotFoundException;


    /**
     * @return the number of customers within the database
     */
    int getNumberOfCustomers ();
}
