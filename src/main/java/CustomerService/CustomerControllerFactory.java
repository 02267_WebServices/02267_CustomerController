package CustomerService;

import messaging.rabbitmq.RabbitMQAdapter;
import messaging.rabbitmq.EventSender;
import messaging.rabbitmq.RabbitMqListener;
import messaging.rabbitmq.RabbitMqSender;

/**
 * @author August Birk Olsen - s154110
 */
public class CustomerControllerFactory {
    private static CustomerController customerController = null;

    /**
     * @return creates a RabbitMQ adapter, as well as creating and returning a Customer Controller object.
     */
    public CustomerController getCustomerController() {
        if (customerController != null) return customerController;

        EventSender sender = new RabbitMqSender();
        CustomerDatabase customers = new CustomerDatabase();
        RabbitMQAdapter rabbitMQAdapter = new RabbitMQAdapter(sender);
        customerController = new CustomerController(customers, rabbitMQAdapter);
        rabbitMQAdapter.setController(customerController);
        RabbitMqListener listener = new RabbitMqListener(rabbitMQAdapter);
        try {
            listener.listen();
        } catch (Exception e) {
            throw new Error(e);
        }
        return customerController;
    }
}
