package CustomerService;

import CustomerService.exceptions.CustomerNotFoundException;
import messaging.rabbitmq.RabbitMQAdapter;

import java.util.List;

/**
 * @author August Birk Olsen - s154110
 */
public class CustomerController implements ICustomerController {
    private CustomerDatabase customers;
    private RabbitMQAdapter rabbitMQAdapter;

    /**
     * @param customers the in-memory database of customers
     * @param rabbitMQAdapter the object for handling RabbitMQ events
     */
    public CustomerController(CustomerDatabase customers, RabbitMQAdapter rabbitMQAdapter) {
        this.customers = customers;
        this.rabbitMQAdapter = rabbitMQAdapter;
    }

    /**
     * @param cpr the cpr number of a customer
     * @param firstName the first name of a customer
     * @param lastName the last name of a customer
     * @param bankAccount the bank account identification of a customer
     * @return the UUID of a customer
     */
    public String createCustomer(String cpr, String firstName, String lastName, String bankAccount) {
        Customer customer = new Customer(cpr, firstName, lastName, bankAccount);
        customers.addCustomer(customer);
        return customer.getId();
    }

    /**
     * @param id UUID of a customer
     * @throws CustomerNotFoundException a customer is not found
     */
    public void deleteCustomer(String id) throws CustomerNotFoundException {
        customers.removeCustomer(id);
    }

    /**
     * @param id the UUID of a customer
     * @return the customer object from the database
     * @throws CustomerNotFoundException a customer is not found
     */
    public Customer getCustomer(String id) throws CustomerNotFoundException {
        return customers.getCustomer(id);
    }

    /**
     * @param id UUID of a customer
     * @param numTokens amount of tokens requested
     * @return list of token UUIDs
     * @throws Exception Exception
     */
    public List<String> requestTokens(String id, int numTokens) throws Exception {
        return rabbitMQAdapter.requestTokens(id, numTokens);
    }
}
